#!/bin/sh

MVN_VERSION=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive exec:exec)
MVN_ARTEFACT=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.name}' --non-recursive exec:exec)
IMAGE="panitz/$MVN_ARTEFACT:$MVN_VERSION"

PRIVATE_KEY=`cat ssh/config-server.rsa`

docker network create spring 2>/dev/null && true
docker rm $MVN_ARTEFACT --force 2>/dev/null && true
docker run -d --rm \
  --name $MVN_ARTEFACT \
  --network="spring" \
  -p 8888:8888 \
  -e SPRING_CLOUD_CONFIG_SERVER_GIT_URI=git@bitbucket.org:panitz/config-server-repo.git \
  -e SPRING_CLOUD_CONFIG_SERVER_GIT_REFRESHRATE=30 \
  -e SPRING_CLOUD_CONFIG_SERVER_GIT_IGNORELOCALSSHSETTINGS=true \
  -e SPRING_CLOUD_CONFIG_SERVER_GIT_PRIVATEKEY="$PRIVATE_KEY" \
  -e SPRING_CLOUD_BUS_ENABLED=true \
  -e SPRING_RABBITMQ_HOST=rabbitmq \
  -e SPRING_RABBITMQ_PORT=5672 \
  -e SPRING_RABBITMQ_VIRTUALHOST=/ \
  -e SPRING_RABBITMQ_USERNAME=user \
  -e SPRING_RABBITMQ_PASSWORD=password \
  -e ENCRYPT_KEY=my-secret-password \
  $IMAGE