# Spring Cloud Config Server Implementation

Die Implementation besitzt aktuell die Voraussetzungen, um [Push Notifications and Spring Cloud Bus](https://cloud.spring.io/spring-cloud-config/reference/html/#_push_notifications_and_spring_cloud_bus)
zu nutzen. Hierfür muss die Verbindung zu einem RabbitMQ-Server konfiguriert werden. Geschieht dieses nicht, dann 
wird trotzdem versucht, einen RabbitMQ-Server zu erreichen und es kommt zu einer Exception.

Um dieses zu verhindern muss Spring Clound Bus und die Health-Abfragen an den RabbitMQ-Server
deaktiviert werden:

```properties
spring.autoconfigure.exclude: org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration
spring.cloud.bus.enabled: false
```