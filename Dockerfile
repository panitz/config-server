FROM maven:3-jdk-8-openj9 AS BUILD
COPY pom.xml .
COPY src src
RUN mvn clean package

FROM openjdk:8-jdk-alpine

RUN addgroup -S spring \
    && adduser -S spring -G spring

COPY --from=BUILD target/config-server-*.jar /app/app.jar
USER spring

CMD ["java", "-jar", "/app/app.jar"]
