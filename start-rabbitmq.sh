#!/bin/sh

IMAGE="rabbitmq:3.8"
CONTAINER=rabbitmq

docker pull $IMAGE
docker network create spring 2>/dev/null
docker rm $CONTAINER --force 2>/dev/null

docker run -d --rm \
  --name $CONTAINER \
  --hostname $CONTAINER \
  --network="spring" \
  -p 5672:5672 \
  -e RABBITMQ_DEFAULT_USER=user \
  -e RABBITMQ_DEFAULT_PASS=password \
  $CONTAINER